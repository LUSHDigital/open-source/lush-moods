from flask import Flask, send_file
app = Flask(__name__)

from phue import Bridge
from time import sleep

b = Bridge('10.10.3.137')

# The button on the Hue Bridge will need to be pressed < 30 seconds before this function is run.
b.connect()

@app.route("/")
def main():
    index_path = os.path.join(app.static_folder, 'index.html')
    return send_file(index_path)

@app.route('/on')
def on():
    b.set_light(1,'on', True)
    b.set_light(2,'on', True)
    b.set_light(3,'on', True)
    return "nothing"

@app.route('/off')
def off():
    b.set_light(1,'on', False)
    b.set_light(2,'on', False)
    b.set_light(3,'on', False)
    return "nothing"

@app.route('/water')
def water():
    b.set_light(1, 'hue', 46920, transitiontime=10)
    b.set_light(2, 'hue', 12750, transitiontime=10)
    b.set_light(3, 'hue', 56100, transitiontime=10)
    return "nothing"

# Everything not declared before (not a Flask route / API endpoint)...
@app.route('/<path:path>')
def route_frontend(path):
    file_path = os.path.join(app.static_folder, path)
    if os.path.isfile(file_path):
        return send_file(file_path)
    else:
        index_path = os.path.join(app.static_folder, 'index.html')
        return send_file(index_path)


if __name__ == "__main__":
    # Only for debugging while developing
    app.run(host='0.0.0.0', debug=True, port=80)
