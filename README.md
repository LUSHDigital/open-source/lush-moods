# Docker Compose

`docker-compose build`

*Push the button on the Hue Bridge*

`docker-compose up`

*visit 0.0.0.0 in your browser*

# Manually

`docker build -t lush-moods .`

*Push the button on the Hue Bridge*

`docker run -it --name moods-container -p 80:80 lush-moods`

*visit 0.0.0.0 in your browser*
